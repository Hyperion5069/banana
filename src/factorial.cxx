#include <iostream>

/**
 * @brief Factorial
 *
 * This subroutine computes the factorial of the given integer \f$n\f$
 * recursively following the definition given in \cite wiki:factorial
 *
 * \f[
 *     n! = \prod_{k=1}^n k
 * \f]
 *
 * @param[in]   n   The number of which the factorial is computed
 *
 * @return          The factorial of n
 */
int factorial(int n)
{
    if (n==1)
        return 1;
    else
        return n*factorial(n-1);
}

/**
 * @brief Main program
 *
 * @remark Returns 0 if factorial(6) is calculated correctly and 1
 *         otherwise
 */
int main() 
{
    return factorial(6) == 720 ? 0 : 1;
}
