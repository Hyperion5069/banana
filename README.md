# The baNaNa project

This repository serves as demo project for the baNaNa talk for the
SIAM Student Chapter Delft given at TU Delft on 8-11-2017.

Gitlab repository http://gitlab.com/mmoelle1/banana  
[![pipeline status](https://gitlab.com/mmoelle1/banana/badges/master/pipeline.svg)](https://gitlab.com/mmoelle1/banana/commits/master)

Gitlab pages: http://mmoelle1.gitlab.io/banana/

# Tutorial 1: Getting started with CI

To reproduce the setup shown during the talk please follow these
steps:

1.  Create a free account at https://www.gitlab.com and sign in

2.  Create a new project `banana` at https://www.gitlab.com

3.  Create a `README.md` file as landing page. It is a good practice to use
    markdown syntax. A very good markdown primer is:
    https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet

4.  Create directory `src` where you will store the source code files

5.  Create a C++ source code file that calculates the factorial of an integer n
    recursively and verifies that the calculation is done correctly.

    ```cpp
    #include <iostream>

    /**
     * @brief Factorial
     *
     * This subroutine computes the factorial of the given integer \f$n\f$
     * recursively following the definition given in \cite wiki:factorial
     *
     * \f[
     *     n! = \prod_{k=1}^n k
     * \f]
     *
     * @param[in]   n   The number of which the factorial is computed
     *
     * @return          The factorial of n
     */
    int factorial(int n)
    {
        if (n==1)
            return 1;
        else
            return n*factorial(n-1);
    }

    /**
     * @brief Main program
     *
     * @remark Returns 0 if factorial(6) is calculated correctly and 1
     *         otherwise
     */
    int main() 
    {
        return factorial(6) == 720 ? 0 : 1;
    }
    ```
    
6. Create a `CMakeLists.txt` file in the top-level directory

    ```cmake
    # Define minimal CMake version
    cmake_minimum_required (VERSION 2.8)

    # Define baNaNa C/C++ project
    project (baNaNa C CXX)

    # Enable testing
    include (CTest)
    enable_testing()

    # Enable Doxygen
    find_package(Doxygen QUIET)
    if(DOXYGEN_FOUND)
        configure_file(${CMAKE_CURRENT_SOURCE_DIR}/doc/Doxyfile.in ${CMAKE_CURRENT_BINARY_DIR}/doc/Doxyfile @ONLY)
        add_custom_target(doc
          ${DOXYGEN_EXECUTABLE} ${CMAKE_CURRENT_BINARY_DIR}/doc/Doxyfile
          WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/doc
        )
    endif()

    # Recurse into the "src" subdirectory.
    add_subdirectory (src)
    ```

7.  Create a `CMakeLists.txt` file in the `src` directory

    ```cmake
    # Add executable called "factorial" built from the source files "factorial.cxx" 
    add_executable (factorial factorial.cxx)

    # Add test called "factorial" based on the executable "factorial"
    add_test(factorial factorial)
    ```

8.  Create a `.gitlab-ci.yml` file in the top-level directory. A tutorial on how
    to write this file is available at https://docs.gitlab.com/ee/ci/yaml/README.html

    ```yaml
    # Use Ubuntu 16.04 Docker image
    image: ubuntu:16.04

    # Install CMake and GCC before all other actions
    before_script:
        - apt-get update -qq && apt-get install -y -qq cmake doxygen gcc g++ graphviz
  
    # Define different stages of the CI process
    stages:
        - build
  
    tests:
        stage: build
        script:
            - mkdir build
            - cd build
            - cmake ..
            - make
            - make test
  
    pages:
        stage: build
        script:
            - mkdir build
            - cd build
            - cmake ..
            - make doc
            - mv doc/html ../public
        artifacts:
            paths:
                - public
        only:
            - master
    ```

# Tutorial 2: Customized Docker images

The re-creation of customized Docker images using `before_script` is
very time consuming and can become very costly if CPU time has to be
paid for. It is therefore a good idea to generate a customized Docker
image once and for all and store it permanently.

## Strategy A: Use Docker images from https://hub.docker.com

Search through the list of freely available Docker images on Docker
Hub. For instance, the Docker image `gcc:latest` provides the latest
version (7.x at the time of writing) of the GNU Compiler
Collection. In order to use it instead of the manually created Docker
image change the file `.gitlab-ci.yml` as follows:

```yaml
# Define different stages of the CI process
stages:
    - build

tests:
    image: gcc:latest
    stage: build
    script:
        - mkdir build
        - cd build
        - cmake ..
        - make
        - make test
    ...
```

## Strategy B: Create customized Docker image at https://hub.Docker.com

If none of the provided Docker images meets your requirements, you can
sign-up at Docker Hub and create your own Docker image. Check for
instance https://hub.docker.com/r/mmoelle1/gismo/

Note that you can have only one private Docker image without having to
pay for the extra service. Private Docker images can contain licensed
software, which are are allowed to use but not allowed to make
publicably available.


# Strategy C: Create customized Docker image at the [GitLab Container
  Registry](https://gitlab.com/help/user/project/container_registry)

GitLab allows you to store your own Docker images in the so-called
Container Registry and use it, e.g., as Docker images in CI pipelines
but also as regular Docker containers on your computer.

As a starting point have a look at my publicly available Docker images:

- https://gitlab.com/mmoelle1/docker-clang3.8
- https://gitlab.com/mmoelle1/docker-clang3.9
- https://gitlab.com/mmoelle1/docker-clang4
- https://gitlab.com/mmoelle1/docker-clang5
- https://gitlab.com/mmoelle1/docker-gcc4.9
- https://gitlab.com/mmoelle1/docker-gcc5
- https://gitlab.com/mmoelle1/docker-gcc6
- https://gitlab.com/mmoelle1/docker-gcc7

Each of these repositories consists of essentially two files.

The `Dockerfile` contains the description of the customized Docker image

```docker
FROM ubuntu:17.04

MAINTAINER Matthias Moeller <mmoelle1@gmail.com>

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update -q && \
    apt-get install -yq software-properties-common && \
    add-apt-repository -y ppa:ubuntu-toolchain-r/test && \
    apt-get update -q && \
    apt-get install --no-install-recommends -yq \
    alien \
    ca-certificates \
    clinfo \
    cmake \
    doxygen \
    fftw-dev \
    gcc-7 \
    g++-7 \
    git \
    graphviz \
    libarrayfire-cpu-dev \
    libblas-dev \
    libboost-all-dev \
    liblapack-dev \
    mesa-common-dev \
    ninja-build \
    subversion \
    unzip \
    wget && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Initialize git command
RUN git config --global user.email "mmoelle1@gmail.com"
RUN git config --global user.name "Matthias Moeller"

# Download and install the Intel OpenCL driver
RUN export OPENCL_DRIVER_URL="http://registrationcenter-download.intel.com/akdlm/irc_nas/9019/opencl_runtime_16.1.1_x64_rh_6.4.0.25.tgz" && \
    export TAR=$(basename ${OPENCL_DRIVER_URL}) && \
    export DIR=$(basename ${OPENCL_DRIVER_URL} .tgz) && \
    wget -q ${OPENCL_DRIVER_URL} && \
    tar -xf ${TAR} && \
    sed -i 's/ACCEPT_EULA=decline/ACCEPT_EULA=accept/g' ${DIR}/silent.cfg && \
    sh ${DIR}/install.sh --silent ${DIR}/silent.cfg --cli-mode && \
    rm -rf ${TAR} ${DIR}
    
# Download and install the Intel OpenCL SDK for Ubuntu
RUN export OPENCL_SDK_URL="http://registrationcenter-download.intel.com/akdlm/irc_nas/vcp/11060/intel_sdk_for_opencl_2016_ubuntu_6.3.0.1904_x64.tgz" && \
    export TAR=$(basename ${OPENCL_SDK_URL}) && \
    export DIR=$(basename ${OPENCL_SDK_URL} .tgz) && \
    wget -q ${OPENCL_SDK_URL} && \
    tar -xf ${TAR} && \
    sed -i 's/ACCEPT_EULA=decline/ACCEPT_EULA=accept/g' ${DIR}/silent.cfg && \
    sh ${DIR}/install.sh --silent ${DIR}/silent.cfg --cli-mode && \
    rm -rf ${TAR} ${DIR}

# Let the system know where to find the OpenCL library at runtime
ENV INTELOCLSDKROOT /opt/intel/opencl
ENV LIB ${INTELOCLSDKROOT}/lib64:$LIB
ENV LD_LIBRARY_PATH ${INTELOCLSDKROOT}/lib64:$LD_LIBRARY_PATH
ENV OpenCL_INCLUDE_DIR ${INTELOCLSDKROOT}/include
ENV OpenCL_LIBRARY ${INTELOCLSDKROOT}/lib64

```

The `.gitlab-ci.yml` file triggers the automatic generation of the
Docker image and its storage in the GitLab Container Registry

```
stages:
    - build
    
gcc7:
    stage: build
    image: docker:git
    services:
        - docker:dind
    script:
        - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN registry.gitlab.com
        - docker build -t registry.gitlab.com/mmoelle1/docker-gcc7:latest .
        - docker push registry.gitlab.com/mmoelle1/docker-gcc7:latest
    only:
        - master
```

In order to use the customized Docker image from the GitLab Container
Registry instead of the manually created Docker image change the file
`.gitlab-ci.yml` of the `banana` project as follows:

```yaml
# Define different stages of the CI process
stages:
    - build

tests:
    stage: build
    image: registry.gitlab.com/mmoelle1/docker-gcc7:latest
    script:
        - mkdir build
        - cd build
        - cmake ..
        - make
        - make test
    ...
```
